package main

import (
		"exp/gui"
		"exp/gui/x11"
		"image"
)

func main() {
		w,_ := x11.NewWindow()
		ec := w.EventChan()
		i := w.Screen()
		c := image.RGBAColor{255,255,255}
		for true {
				e := <- ec
				switch v := e.(type) {
				case gui.MouseEvent:
						i.Set(e.(gui.MouseEvent).Loc.X, e.(gui.MouseEvent).Loc.Y, c)
				}
				w.FlushImage()
		}
}
