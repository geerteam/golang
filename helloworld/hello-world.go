package main

import "fmt"

func hello_str() (string) {
	return "hello world"
}

func main() {
    fmt.Println(hello_str())
}

